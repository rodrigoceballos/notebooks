import argparse

parser = argparse.ArgumentParser(
    description='MIL maxpool training given tweaked version of openslide')
# I/O PARAMS
#parser.add_argument('--train_list', type=str, default='',
#                    help='path to train MIL file list')
parser.add_argument('--dir_path', type=str, default='new_hvd_results/tile_probs_vis/',
                    help='dir path to save images')

parser.add_argument('--margin', type=int, default=1000,
                    help='Margin around images')

parser.add_argument('--slides', type=list,
                    help='list of slides to visualize')


slide_list = ['b0e553889efa563836e49e43f49afce3',
 '49c83c38b837cc49e21d98e870a2f785',
 'c27e70c2b74fca5ba56fe715a44b3e95']


def main():
    global args
    args = parser.parse_args()

    margin = 1000
    fn_dir_path = Path(dir_path, 'fn')
    for slide_name in tqdm_notebook(fn):
        print(slide_name)    
        visualize_bounding_boxes(DL, slide_name, margin)

    


if __name__ == '__main__':
    main()